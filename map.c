/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:28:41 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 12:04:05 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	error_in_map(char *str)
{
	perror(str);
	exit(1);
}

static void	print_the_map(t_env *env)
{
	int	i;

	i = 0;
	while (env->data->map[i])
	{
		ft_putendl(env->data->map[i]);
		i++;
	}
}

static void	get_the_line(char *line, t_data *data, int n)
{
	int		j;

	j = 0;
	while (line[j])
	{
		data->map[n][j] = line[j];
		j++;
	}
	data->map[n][MAP_WIDTH] = 0;
}

static void	read_the_map(int fd, t_env *env)
{
	int		i;
	int		ret;
	char	*line;

	if ((env->data->map = (char **)malloc(sizeof(char *) * MAP_HEIGHT + 1))\
																	== NULL)
		error_in_map("Error: malloc data.map");
	if ((line = (char *)malloc(sizeof(char) * MAP_WIDTH + 1)) == NULL)
		error_in_map("Error: malloc line");
	i = 0;
	while (i < MAP_HEIGHT)
	{
		ret = 0;
		if ((env->data->map[i] = (char *)malloc(sizeof(char) * MAP_WIDTH + 1))\
																	== NULL)
			error_in_map("Error: malloc data.map");
		if ((ret = read(fd, line, MAP_WIDTH + 1)) > 0)
			get_the_line(line, env->data, i);
		else
			error_in_map("Error: read");
		i++;
	}
	free(line);
	line = NULL;
	env->data->map[MAP_HEIGHT] = 0;
}

void		open_the_map(char *file, t_env *env)
{
	int fd;

	if ((fd = open(file, O_RDONLY)) == -1)
	{
		perror("Error: open");
		exit(1);
	}
	read_the_map(fd, env);
	if (close(fd) == -1)
	{
		perror("Error: close");
		clean_and_quit(env);
	}
	print_the_map(env);
}
