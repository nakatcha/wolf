/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:27:12 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 11:56:01 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	init_map_view(t_data **data)
{
	int	i;
	int	full;

	i = 0;
	full = MAP_WIDTH * MAP_HEIGHT;
	if (((*data)->map_view = (int *)malloc(sizeof(int) * \
								(MAP_WIDTH * MAP_HEIGHT))) == NULL)
	{
		perror("Error: malloc data->view_map");
		exit(1);
	}
	while (i < full)
	{
		(*data)->map_view[i] = 2;
		i++;
	}
}

void		init_img(t_env *env)
{
	if ((env->img = (t_img *)malloc(sizeof(t_img))) == NULL)
	{
		perror("Error: malloc env->img");
		exit(1);
	}
	if ((env->image = mlx_new_image(env->init, IMG_WIDTH, IMG_HEIGHT)) == NULL)
	{
		perror("Error: mlx new image");
		exit(1);
	}
	env->img->data = mlx_get_data_addr(env->image, &env->img->bpp,\
	&env->img->size_line, &env->img->endian);
	env->img->width = IMG_WIDTH;
	env->img->height = IMG_HEIGHT;
}

t_data		*malloc_data(t_data *data)
{
	if ((data = (t_data *)malloc(sizeof(t_data))) == NULL)
	{
		perror("Error: malloc t_data");
		exit(1);
	}
	data->camera_x = 0;
	data->ray_pos_x = MAP_WIDTH / 2;
	data->ray_pos_y = MAP_HEIGHT / 2;
	data->ray_dir_x = 0;
	data->ray_dir_y = 0;
	data->start_y = 0;
	data->end_y = 0;
	data->color = 0x000000;
	init_map_view(&data);
	return (data);
}

t_player	*malloc_player(t_player *player)
{
	if ((player = (t_player *)malloc(sizeof(t_player))) == NULL)
	{
		perror("Error: malloc t_player");
		exit(1);
	}
	player->map_x = MAP_WIDTH / 2;
	player->map_y = MAP_HEIGHT / 2;
	player->pos_x = MAP_WIDTH / 2;
	player->pos_y = MAP_HEIGHT / 2;
	player->dir_x = -1;
	player->dir_y = 0;
	player->plane_x = 0;
	player->plane_y = 0.66;
	return (player);
}

void		init_env(t_env *env)
{
	init_img(env);
	env->show_map = 0;
	env->print = 0;
}
