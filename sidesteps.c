/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sidesteps.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 12:24:48 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 12:24:54 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	sidestep_left(t_env *env)
{
	authorized_move_x(env->player->pos_x + -env->player->plane_x * MOVE, env);
	authorized_move_y(env->player->pos_y + -env->player->plane_y * MOVE, env);
}

void	sidestep_right(t_env *env)
{
	authorized_move_x(env->player->pos_x + env->player->plane_x * MOVE, env);
	authorized_move_y(env->player->pos_y + env->player->plane_y * MOVE, env);
}
