/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:29:55 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 12:25:10 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	free_map(t_env *env)
{
	int	i;

	i = 0;
	while (env->data->map[i])
		i++;
	while (i-- > 0)
	{
		ft_bzero(env->data->map[i], ft_strlen(env->data->map[i]));
		free(env->data->map[i]);
	}
	free(env->data->map);
	env->data->map = NULL;
}

void		clean_and_quit(t_env *env)
{
	if (env->player)
		free(env->player);
	env->player = NULL;
	if (env->data)
	{
		if (env->data->map)
			free_map(env);
		if (env->data->map_view)
			free(env->data->map_view);
		free(env->data);
	}
	env->data = NULL;
	env = NULL;
	exit(1);
}
