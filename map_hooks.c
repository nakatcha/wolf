/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_hooks.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:28:50 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 12:13:43 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void		black_box(int x, int y, t_env *env)
{
	int end_x;
	int end_y;
	int save_x;

	end_x = x + IMG_WIDTH / MAP_WIDTH;
	end_y = y + IMG_HEIGHT / MAP_HEIGHT;
	save_x = x;
	while (y < end_y)
	{
		x = save_x;
		while (x < end_x)
		{
			pix_in_img(env->img, x, y, 0x80808080);
			x++;
		}
		y++;
	}
}

static void		put_player(t_env *env, int plus_x, int plus_y)
{
	int	x;
	int	y;
	int	end_x;
	int	end_y;

	y = env->player->pos_y * plus_y;
	x = env->player->pos_x * plus_x;
	end_y = y + 10;
	end_x = x + 10;
	while (y < end_y)
	{
		x = env->player->pos_x * plus_x;
		while (x < end_x)
		{
			pix_in_img(env->img, x - 5, y - 5, -1);
			pix_in_img(env->img, x - 5, y - 5, 0x800000B3);
			x++;
		}
		y++;
	}
}

void			draw_map(t_env *env)
{
	int	i;
	int	j;
	int	place_x;
	int	place_y;

	i = -1;
	place_x = 0;
	place_y = 0;
	while (env->data->map[++i])
	{
		j = -1;
		place_x = 1;
		while (env->data->map[i][++j])
		{
			if (env->data->map[i][j] > '0' && env->data->map[i][j] <= '9' \
					&& env->data->map_view[i * MAP_WIDTH + j] \
					&& env->data->map_view[i * MAP_WIDTH + j] == 1)
				black_box(place_x, place_y, env);
			place_x += IMG_WIDTH / MAP_WIDTH;
		}
		place_y += IMG_HEIGHT / MAP_HEIGHT;
	}
	put_player(env, IMG_WIDTH / MAP_WIDTH, IMG_HEIGHT / MAP_HEIGHT);
}

void			show_me_the_map_dude(t_env *env)
{
	if (env->show_map == 0)
		env->show_map = 1;
	else
		env->show_map = 0;
	expose_hook(env);
}
