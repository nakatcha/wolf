/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rays.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:29:48 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 12:24:31 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	try_raycast(t_env *env)
{
	int		x;
	double	perp_wall_dist;
	int		line_height;
	int		draw_start;
	int		draw_end;

	x = 0;
	while (x < IMG_WIDTH)
	{
		get_datas(env, x);
		get_directions(env);
		if (env->player->side == 0)
			perp_wall_dist = fabs((env->player->map_x - env->player->pos_x \
					+ (1 - env->player->more_x) / 2) / env->data->ray_dir_x);
		else
			perp_wall_dist = fabs((env->player->map_y - env->player->pos_y \
					+ (1 - env->player->more_y) / 2) / env->data->ray_dir_y);
		line_height = abs((int)(IMG_HEIGHT / perp_wall_dist));
		draw_start = IMG_HEIGHT / 2 - line_height / 2;
		draw_end = line_height / 2 + IMG_HEIGHT / 2;
		vert_line(x, draw_start, draw_end, env);
		x++;
	}
}
