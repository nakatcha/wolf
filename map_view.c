/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_view.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/24 12:13:52 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 12:16:54 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	fill_the_corners(t_env *env)
{
	if (env->data->map_view[1] == 1 && env->data->map_view[MAP_WIDTH] == 1)
		env->data->map_view[0] = 1;
	if (env->data->map_view[MAP_WIDTH * MAP_HEIGHT - 2] == 1 && \
			env->data->map_view[MAP_WIDTH * (MAP_HEIGHT - 1) - 1] == 1)
		env->data->map_view[MAP_WIDTH * MAP_HEIGHT - 1] = 1;
	if (env->data->map_view[MAP_WIDTH - 2] == 1 && \
			env->data->map_view[MAP_WIDTH * 2 - 1] == 1)
		env->data->map_view[MAP_WIDTH - 1] = 1;
	if (env->data->map_view[MAP_WIDTH * (MAP_HEIGHT - 2)] == 1 && \
			env->data->map_view[MAP_WIDTH * (MAP_HEIGHT - 1) + 1] == 1)
		env->data->map_view[MAP_WIDTH * (MAP_HEIGHT - 1)] = 1;
}

void	change_map_view(t_env *env)
{
	if (env->data->map_view[(int)env->player->pos_y * MAP_WIDTH + \
													(int)env->player->pos_x])
	{
		env->data->map_view[(int)env->player->pos_y * MAP_WIDTH + \
									(int)env->player->pos_x] = 1;
		if (env->data->map_view[(int)env->player->pos_y * MAP_WIDTH + \
									(int)env->player->pos_x + 1])
			env->data->map_view[(int)env->player->pos_y * MAP_WIDTH + \
									(int)env->player->pos_x + 1] = 1;
		if (env->data->map_view[(int)env->player->pos_y * MAP_WIDTH + \
									(int)env->player->pos_x - 1])
			env->data->map_view[(int)env->player->pos_y * MAP_WIDTH + \
									(int)env->player->pos_x - 1] = 1;
		if (env->data->map_view[(int)env->player->pos_y * MAP_WIDTH + \
									(int)env->player->pos_x + MAP_WIDTH])
			env->data->map_view[(int)env->player->pos_y * MAP_WIDTH +\
									(int)env->player->pos_x + MAP_WIDTH] = 1;
		if (env->data->map_view[(int)env->player->pos_y * MAP_WIDTH + \
									(int)env->player->pos_x - MAP_WIDTH])
			env->data->map_view[(int)env->player->pos_y * MAP_WIDTH + \
									(int)env->player->pos_x - MAP_WIDTH] = 1;
	}
	fill_the_corners(env);
}
