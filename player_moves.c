/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   player_moves.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:29:21 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 12:21:58 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void		authorized_move_x(double new_pos, t_env *env)
{
	if (new_pos >= MAP_WIDTH)
		new_pos = MAP_WIDTH - 1;
	else if (new_pos < 0)
		new_pos = 0;
	if (env->data->map[(int)env->player->pos_y][(int)new_pos] && \
			env->data->map[(int)env->player->pos_y][(int)new_pos] == '0')
		env->player->pos_x = new_pos;
	else
		ft_putendl("can't do this move");
	change_map_view(env);
}

void		authorized_move_y(double new_pos, t_env *env)
{
	if (new_pos >= MAP_HEIGHT)
		new_pos = MAP_HEIGHT - 1;
	else if (new_pos < 0)
		new_pos = 0;
	if (env->data->map[(int)new_pos][(int)env->player->pos_x] && \
			env->data->map[(int)new_pos][(int)env->player->pos_x] == '0')
		env->player->pos_y = new_pos;
	else
		ft_putendl("can't do this move");
	change_map_view(env);
}

static void	rotate_left(t_env *env)
{
	double old_dir_x;
	double old_plane_x;

	old_dir_x = env->player->dir_x;
	env->player->dir_x = env->player->dir_x * cos(ROTATE) \
							- env->player->dir_y * sin(ROTATE);
	env->player->dir_y = old_dir_x * sin(ROTATE) \
							+ env->player->dir_y * cos(ROTATE);
	old_plane_x = env->player->plane_x;
	env->player->plane_x = env->player->plane_x * cos(ROTATE) \
							- env->player->plane_y * sin(ROTATE);
	env->player->plane_y = old_plane_x * sin(ROTATE) \
							+ env->player->plane_y * cos(ROTATE);
}

static void	rotate_right(t_env *env)
{
	double old_dir_x;
	double old_plane_x;

	old_dir_x = env->player->dir_x;
	env->player->dir_x = env->player->dir_x * cos(-ROTATE) \
							- env->player->dir_y * sin(-ROTATE);
	env->player->dir_y = old_dir_x * sin(-ROTATE) \
							+ env->player->dir_y * cos(-ROTATE);
	old_plane_x = env->player->plane_x;
	env->player->plane_x = env->player->plane_x * cos(-ROTATE) \
							- env->player->plane_y * sin(-ROTATE);
	env->player->plane_y = old_plane_x * sin(-ROTATE) \
							+ env->player->plane_y * cos(-ROTATE);
}

void		move_player(t_env *env, int keycode)
{
	if (keycode == 65364 || keycode == 126)
	{
		authorized_move_x(env->player->pos_x + env->player->dir_x * MOVE, env);
		authorized_move_y(env->player->pos_y + env->player->dir_y * MOVE, env);
	}
	else if (keycode == 65363 || keycode == 125)
	{
		authorized_move_x(env->player->pos_x - env->player->dir_x * MOVE, env);
		authorized_move_y(env->player->pos_y - env->player->dir_y * MOVE, env);
	}
	else if (keycode == 65361 || keycode == 123)
		rotate_left(env);
	else if (keycode == 65362 || keycode == 124)
		rotate_right(env);
	else if (keycode == 12)
		sidestep_left(env);
	else if (keycode == 13)
		sidestep_right(env);
	expose_hook(env);
}
