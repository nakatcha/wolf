/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   directions.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:27:42 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 11:54:43 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void	get_datas(t_env *env, int x)
{
	env->data->camera_x = 2 * x / (double)IMG_WIDTH - 1;
	env->data->ray_pos_x = env->player->pos_x;
	env->data->ray_pos_y = env->player->pos_y;
	env->data->ray_dir_x = env->player->dir_x + env->player->plane_x * \
							env->data->camera_x;
	env->data->ray_dir_y = env->player->dir_y + env->player->plane_y * \
							env->data->camera_x;
	env->player->map_x = (int)env->data->ray_pos_x;
	env->player->map_y = (int)env->data->ray_pos_y;
	env->data->dist_xa = sqrt(1 + (env->data->ray_dir_y * \
							env->data->ray_dir_y) / (env->data->ray_dir_x * \
							env->data->ray_dir_x));
	env->data->dist_ya = sqrt(1 + (env->data->ray_dir_x * \
							env->data->ray_dir_x) / (env->data->ray_dir_y * \
							env->data->ray_dir_y));
}

void	get_directions(t_env *env)
{
	if (env->data->ray_dir_x > 0)
	{
		env->player->more_x = 1;
		env->player->dist_x = (env->player->map_x + 1.0 - \
								env->data->ray_pos_x) * env->data->dist_xa;
	}
	else
	{
		env->player->more_x = -1;
		env->player->dist_x = (env->data->ray_pos_x - env->player->map_x) * \
								env->data->dist_xa;
	}
	if (env->data->ray_dir_y > 0)
	{
		env->player->more_y = 1;
		env->player->dist_y = (env->player->map_y + 1.0 - \
								env->data->ray_pos_y) * env->data->dist_ya;
	}
	else
	{
		env->player->more_y = -1;
		env->player->dist_y = (env->data->ray_pos_y - env->player->map_y) * \
								env->data->dist_ya;
	}
	dda(env);
}
