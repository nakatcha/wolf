/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libx_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:28:21 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 11:59:52 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

int		move_hook(int keycode, t_env *env)
{
	if ((keycode >= 65361 && keycode <= 65364) || \
			(keycode >= 123 && keycode <= 126) || \
			keycode == 12 || keycode == 13)
		move_player(env, keycode);
	return (0);
}

int		key_hook(int keycode, t_env *env)
{
	ft_putnbr(keycode);
	ft_putchar('\n');
	if (keycode == 65307 || keycode == 53)
	{
		mlx_destroy_image(env->init, env->image);
		mlx_destroy_window(env->init, env->window);
		clean_and_quit(env);
	}
	if (keycode == 46)
		show_me_the_map_dude(env);
	if (keycode == 4)
		change_print_status(env);
	return (0);
}

int		expose_hook(t_env *env)
{
	try_raycast(env);
	mlx_put_image_to_window(env->init, env->window, env->image, 0, 0);
	if (env->show_map == 1)
	{
		draw_map(env);
		mlx_put_image_to_window(env->init, env->window, env->image, 0, 0);
	}
	if (env->print == 1)
		print_help(env);
	return (0);
}

void	change_print_status(t_env *env)
{
	if (env->print == 0)
		env->print = 1;
	else
		env->print = 0;
	expose_hook(env);
}

void	print_help(t_env *env)
{
	mlx_string_put(env->init, env->window, 1, 1, 0xFFFFFF, \
													"PRESS M = MAP");
	mlx_string_put(env->init, env->window, 1, 15, 0xFFFFFF, \
													"PRESS ESC = QUIT");
	mlx_string_put(env->init, env->window, 1, 30, 0xFFFFFF, \
													"UP AND DOWN = MOVE");
	mlx_string_put(env->init, env->window, 1, 45, 0xFFFFFF, \
													"LEFT AND RIGHT = ROTATE");
	mlx_string_put(env->init, env->window, 1, 60, 0xFFFFFF, \
													"Q AND W = SIDESTEPS");
}
