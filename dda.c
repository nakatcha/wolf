/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dda.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:27:42 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 11:40:44 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	dda_x(t_env *env, int *map_x)
{
	*map_x += env->player->more_x;
	env->player->map_x += env->player->more_x;
	env->player->dist_x += env->data->dist_xa;
	env->player->side = 0;
}

static void	dda_y(t_env *env, int *map_y)
{
	*map_y += env->player->more_y;
	env->player->map_y += env->player->more_y;
	env->player->dist_y += env->data->dist_ya;
	env->player->side = 1;
}

static void	change_view(t_env *env, int y, int x)
{
	if (env->data->map_view[y * MAP_WIDTH + x])
		env->data->map_view[y * MAP_WIDTH + x] = 1;
	fill_the_corners(env);
}

void		dda(t_env *env)
{
	int		hit;
	int		map_x;
	int		map_y;

	map_x = (int)env->player->pos_x;
	map_y = (int)env->player->pos_y;
	hit = 0;
	while (hit == 0)
	{
		if (env->player->dist_x < env->player->dist_y)
			dda_x(env, &map_x);
		else
			dda_y(env, &map_y);
		if (env->data->map[map_y][map_x] != '0')
		{
			hit = 1;
			change_view(env, map_y, map_x);
		}
	}
}
