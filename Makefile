NAME = wolf3d

CFLAGS = -Wall -Wextra -Werror 
INCLUDE = -I ./
SRC_PATH = ./
OBJ_PATH = ./
SRC_F =	main.c			\
	dda.c			\
	directions.c		\
	init.c			\
	libx_utils.c		\
	map.c			\
	map_hooks.c		\
	map_view.c		\
	pix.c			\
	player_moves.c		\
	rays.c			\
	sidesteps.c		\
	utils.c

OBJ_F = $(SRC_F:.c=.o)
SRC = $(addprefix $(SRC_PATH), $(SRC_F))
OBJ = $(addprefix $(OBJ_PATH), $(OBJ_F))

all: $(NAME)

$(NAME): $(OBJ)
	make -C libft/
	make -C  mlx/
	gcc -o $(NAME) $(CFLAGS) -framework OpenGL -framework AppKit $(OBJ_F) ./libft/libft.a ./mlx/libmlx.a

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	gcc -o $@ -c $< $(CFLAGS) $(INCLUDE)

clean:
	rm -f $(OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all
