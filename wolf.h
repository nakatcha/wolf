/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:30:06 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 12:00:55 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF_H
# define WOLF_H

# define MAP_HEIGHT 20
# define IMG_HEIGHT 600
# define MAP_WIDTH 20
# define IMG_WIDTH 800
# define MOVE 0.3
# define ROTATE 0.1

# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/uio.h>
# include "libft/libft.h"
# include "mlx/mlx.h"

typedef struct		s_data
{
	char			**map;
	int				*map_view;
	double			camera_x;
	double			ray_pos_x;
	double			ray_pos_y;
	double			ray_dir_x;
	double			ray_dir_y;
	int				start_y;
	int				end_y;
	double			dist_xa;
	double			dist_ya;
	unsigned long	color;
}					t_data;

typedef struct		s_player
{
	double			pos_x;
	double			pos_y;
	int				map_x;
	int				map_y;
	int				more_x;
	int				more_y;
	double			dist_x;
	double			dist_y;
	int				side;
	double			dir_x;
	double			dir_y;
	double			plane_x;
	double			plane_y;
}					t_player;

typedef struct		s_img
{
	double			width;
	double			height;
	int				bpp;
	int				endian;
	int				size_line;
	char			*data;
}					t_img;

typedef struct		s_env
{
	void			*init;
	void			*window;
	void			*image;
	t_img			*img;
	t_data			*data;
	t_player		*player;
	int				show_map;
	int				print;
}					t_env;

void				authorized_move_x(double new_pos, t_env *env);
void				authorized_move_y(double new_pos, t_env *env);
void				change_map_view(t_env *env);
void				change_print_status(t_env *env);
void				clean_and_quit(t_env *env);
void				dda(t_env *env);
void				draw_a_wall(t_env *env);
void				draw_map(t_env *env);
int					expose_hook(t_env *env);
void				fill_the_corners(t_env *env);
void				get_datas(t_env *env, int x);
void				get_directions(t_env *env);
void				init_env(t_env *env);
void				init_img(t_env *env);
int					key_hook(int keycode, t_env *env);
t_data				*malloc_data(t_data *data);
t_player			*malloc_player(t_player *player);
int					move_hook(int keycode, t_env *env);
void				move_player(t_env *env, int keycode);
void				open_the_map(char *file, t_env *env);
void				pix_in_img(t_img *img, int x, int y, unsigned long color);
void				print_help(t_env *env);
void				sidestep_left(t_env *env);
void				sidestep_right(t_env *env);
void				show_me_the_map_dude(t_env *env);
void				try_raycast(t_env *env);
void				vert_line(int x, int draw_start, int draw_end, t_env *env);

#endif
