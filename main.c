/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:27:21 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 11:59:32 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

static void	error_in_main(char *str)
{
	perror(str);
	exit(1);
}

static void	usage(char *argv)
{
	ft_putstr("usage: ");
	ft_putstr(argv);
	ft_putendl(" map.txt");
	exit(1);
}

int			main(int argc, char **argv)
{
	t_env	env;

	if (argc != 2)
		usage(argv[0]);
	else
	{
		env.data = malloc_data(env.data);
		env.player = malloc_player(env.player);
		open_the_map(argv[1], &env);
		if ((env.init = mlx_init()) == NULL)
			error_in_main("Error: mlx init");
		if ((env.window = mlx_new_window(env.init, IMG_WIDTH, IMG_HEIGHT, \
															"Wolf3d")) == NULL)
			error_in_main("Error: mlx window");
		init_env(&env);
		mlx_key_hook(env.window, key_hook, &env);
		mlx_hook(env.window, 2, 1L << 1, move_hook, &env);
		mlx_expose_hook(env.window, expose_hook, &env);
		mlx_loop(env.init);
	}
}
