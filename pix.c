/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pix.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gkervran <gkervran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 15:29:00 by gkervran          #+#    #+#             */
/*   Updated: 2016/01/24 12:19:11 by gkervran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf.h"

void					pix_in_img(t_img *img, int x, int y, \
														unsigned long color)
{
	int		i;
	char	c;

	if (img->data && x <= img->width && x >= 0 && y <= img->height && y >= 0)
	{
		i = x * img->bpp / 8 + y * img->size_line;
		if (img->endian == 0)
		{
			c = (color & 0xFF) >> 0;
			img->data[i] = c;
			c = (color & 0xFF00) >> 8;
			img->data[i + 1] = c;
			c = (color & 0xFF0000) >> 16;
			img->data[i + 2] = c;
			c = (color & 0xFF000000) >> 24;
			img->data[i + 3] = c;
		}
		else
			ft_putendl("endian issue, wrote only for macOS");
	}
}

static unsigned long	wall_color(t_env *env)
{
	unsigned long	color;

	if (env->data->ray_dir_x < 0 && env->player->side == 0)
		color = 0x00FFFFFF;
	else if (env->data->ray_dir_x >= 0 && env->player->side == 0)
		color = 0x00FFFF00;
	else if (env->data->ray_dir_y < 0 && env->player->side == 1)
		color = 0x0000FF00;
	else if (env->data->ray_dir_y >= 0 && env->player->side == 1)
		color = 0x0000FFFF;
	else
		color = 0x00000000;
	return (color);
}

static unsigned long	choose_color(int y, t_env *env)
{
	if (y < env->data->start_y)
		return (0x000000FF);
	else if (y > env->data->end_y)
		return (0x00FF0000);
	else
		return (wall_color(env));
	return (0x00FFFFFF);
}

static void				trace_vert_line(int x, t_env *env)
{
	int	y;

	y = 0;
	while (y < IMG_HEIGHT)
	{
		env->data->color = choose_color(y, env);
		pix_in_img(env->img, x, y, env->data->color);
		y++;
	}
}

void					vert_line(int x, int draw_start, int draw_end, \
																t_env *env)
{
	if (draw_start < 0)
		draw_start = 0;
	else if (draw_start >= IMG_HEIGHT)
		draw_start = IMG_HEIGHT - 1;
	if (draw_end < 0)
		draw_end = 0;
	else if (draw_end >= IMG_HEIGHT)
		draw_end = IMG_HEIGHT - 1;
	env->data->start_y = draw_start;
	env->data->end_y = draw_end;
	trace_vert_line(x, env);
}
